# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table product (
  id                        bigint not null,
  ean                       varchar(255),
  name                      varchar(255),
  description               varchar(255),
  price                     float,
  remain_product            integer,
  image                     bytea,
  constraint pk_product primary key (id))
;

create table tag (
  id                        bigint not null,
  name                      varchar(255),
  constraint pk_tag primary key (id))
;


create table product_tag (
  product_id                     bigint not null,
  tag_id                         bigint not null,
  constraint pk_product_tag primary key (product_id, tag_id))
;
create sequence product_seq;

create sequence tag_seq;




alter table product_tag add constraint fk_product_tag_product_01 foreign key (product_id) references product (id);

alter table product_tag add constraint fk_product_tag_tag_02 foreign key (tag_id) references tag (id);

# --- !Downs

drop table if exists product cascade;

drop table if exists product_tag cascade;

drop table if exists tag cascade;

drop sequence if exists product_seq;

drop sequence if exists tag_seq;

