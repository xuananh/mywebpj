package controllers;

import com.avaje.ebean.Ebean;
import models.Product;
import models.Tag;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.products.details;
import views.html.products.list;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by XUAN_ANH on 4/12/2015.
 */
public class Products extends Controller {
    public static Result list() {
        List<Product> products = Product.findAll();
        return ok(list.render(products));
    }

    private static final Form<Product> productForm = Form.form(Product.class);

    public static Result newProduct() {
        return ok(details.render(productForm));
    }

    public static Result details(Product product) {
        if (product == null) {
            return notFound(String.format("Sản phẩm không tồn tại", product.ean));
        }
        Form<Product> filledForm = productForm.fill(product);
        return ok(details.render(filledForm));
    }

    public static Result delete(String ean) {
        Product product = Product.findByEan(ean);
        if (product == null) {
            return notFound(String.format("Sản phẩm không tồn tại", ean));
        }
        for (Tag tag : product.tags) {
            tag.products.remove(product);
            tag.save();
        }

        product.delete();
        return redirect(routes.Products.list());
    }

    public static Result save() {
        Form<Product> boundForm = productForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("Lỗi", "Hoàn thành form");
            return badRequest(details.render(boundForm));
        }
        Product product = boundForm.get();
        List<Tag> tags = new ArrayList<Tag>();
        for (Tag tag : product.tags) {
            if (tag.id != null) {
                tags.add(Tag.findById(tag.id));
            }
        }
        product.tags = tags;

        if (product.id == null) {
            Ebean.save(product);
        } else {
            if (product.ean != null) {
                flash("Lỗi ,Mã hàng không tồn tại !!!", "Nhập lại mã hàng");
            }
            Ebean.update(product);
        }
        return redirect(routes.Products.list());
    }


}
